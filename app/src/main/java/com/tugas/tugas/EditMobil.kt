package com.tugas.tugas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.SyncStateContract.Helpers.update
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_mobil.*
import kotlinx.android.synthetic.main.activity_invoice.*
import kotlinx.android.synthetic.main.activity_tambah_edit.*

class EditMobil : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_mobil)

        val bundle = intent.extras
        id_edit.setText(bundle?.getString("id"))
        nama_mobil_edit.setText(bundle?.getString("nama"))
        harga_mobile_edit.setText(bundle?.getString("harga"))

        btn_tambah_update.setOnClickListener(){
            updateData()
        }
    }

    private fun updateData() {
        if (nama_mobil_edit.text.toString().trim().isEmpty()){
            nama_mobil_edit.error = "Input Nama Mobile"
            nama_mobil_edit.requestFocus()
            return
        }
        if (harga_mobile_edit.text.toString().isEmpty()){
            harga_mobile_edit.error ="Input Harga Mobile"
            harga_mobile_edit.requestFocus()
            return
        }
        val id = id_edit.text.toString().trim()
        val database = FirebaseDatabase.getInstance()
        val data = DataMobil(id,nama_mobil_edit.text.toString(), harga_mobile_edit.text.toString())

        val myRef = database.getReference("DataMobile")

//        myRef.child(id_mobile.toString()).setValue(data)
        myRef.child(id).setValue(data).addOnCompleteListener {
            Toast.makeText(this, "Sukses Edit data", Toast.LENGTH_LONG).show()
            val intent = Intent(this, TambahEdit::class.java);
            startActivity(intent);
        }
    }
}