package com.tugas.tugas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_data_sewa_cust.*
import kotlinx.android.synthetic.main.activity_invoice.*

class Invoice : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice)
        //terima paket dari Lihatdata
        val bundle = intent.extras

        namapemesan.setText(bundle?.getString("nama"))
        nomerpemesan.setText(bundle?.getString("nohp"))
        tvpesanan.setText(bundle?.getString("jenismobil"))
        tvdurasi.setText(bundle?.getString("hari"))
        tvharga.setText("200000")
        tvketerangan.setText("Langsung")
    }
}