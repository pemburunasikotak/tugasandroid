package com.tugas.tugas

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.log


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_login.setOnClickListener(){
            getUser()
        }

    }

    private fun getUser() {
        if (username.text.toString().trim().isEmpty()){
            username.error = "masukkan UserName"
            username.requestFocus()
            return
        }
        if (password.text.toString().isEmpty()){
            password.error ="masukkan Paswd yang benar"
            password.requestFocus()
            return
        }
        //ahir Falidasi data

        val user =username.text.toString().trim()
        val pasword= password.text.toString().trim()
        Log.e("User", user)
        if(user.equals("admin")){
            Log.e("HEHHE", "HEHE")
            var query = FirebaseDatabase.getInstance().getReference("Admin").orderByChild("username").equalTo(user)
            query.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        Log.e("Test", "Masuk")
                        for (snap in snapshot.children) {
                            val x = snap.getValue(ModelUser::class.java)
                            if (x!!.password.equals(pasword.trim())) {
                                val intent = Intent(this@MainActivity, TambahEdit::class.java)
                                startActivity(intent)
                            } else {
                                Toast.makeText(this@MainActivity, "User Tidak Ditemukan", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }else {
            Log.e("user", user)
            var query = FirebaseDatabase.getInstance().getReference("Member").orderByChild("username").equalTo(user)
            query.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        for (snap in snapshot.children) {
                            val x = snap.getValue(ModelUser::class.java)
                            if (x!!.password.equals(pasword.trim())) {
                                val intent = Intent(this@MainActivity, DataSewaCust::class.java)
                                startActivity(intent)
                            } else {
                                Toast.makeText(this@MainActivity, "User Tidak Ditemukan", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }

    }

}