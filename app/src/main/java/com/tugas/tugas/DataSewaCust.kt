package com.tugas.tugas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_data_sewa_cust.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tambah_edit.*

class DataSewaCust : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_sewa_cust)
        btn_simpan_data_sewa.setOnClickListener() {
            simpan()
        }
    }

    private fun simpan() {
        if (nama_data_sewa.text.toString().trim().isEmpty()){
            nama_data_sewa.error = "masukkan UserName"
            nama_data_sewa.requestFocus()
            return
        }
        if (nohp_data_sewa.text.toString().isEmpty()){
            nohp_data_sewa.error ="masukkan Paswd yang benar"
            nohp_data_sewa.requestFocus()
            return
        }
        if (jenis_mobil_data_sewa.text.toString().isEmpty()){
            jenis_mobil_data_sewa.error ="masukkan Paswd yang benar"
            jenis_mobil_data_sewa.requestFocus()
            return
        }
        if (jenis_sewa_data_sewa.text.toString().isEmpty()){
            jenis_sewa_data_sewa.error ="masukkan Paswd yang benar"
            jenis_sewa_data_sewa.requestFocus()
            return
        }
        if (hari_data_sewa.text.toString().isEmpty()){
            hari_data_sewa.error ="masukkan Paswd yang benar"
            hari_data_sewa.requestFocus()
            return
        }
        val database = FirebaseDatabase.getInstance()
        val id_sewa =database.getReference("DataSewa").push().getKey();
        val datasewa = DataSewa(id_sewa.toString(), nama_data_sewa.text.toString(), nohp_data_sewa.text.toString(),jenis_mobil_data_sewa.text.toString(), jenis_sewa_data_sewa.text.toString().trim(), hari_data_sewa.text.toString().trim())

        val myRef = database.getReference("DataSewa")

        myRef.child(id_sewa.toString()).setValue(datasewa)
        myRef.child(id_sewa.toString()).setValue(datasewa).addOnCompleteListener {
            Toast.makeText(this, "Sukses Tambah data Sewa", Toast.LENGTH_LONG).show()
            val bundel = Bundle()
            bundel.putString("nama", nama_data_sewa.text.toString().trim())
            bundel.putString("nohp", nohp_data_sewa.text.toString().trim())
            bundel.putString("jenismobil",jenis_mobil_data_sewa.text.toString().trim() )
            bundel.putString("jenissewa", jenis_sewa_data_sewa.text.toString().trim())
            bundel.putString("hari", hari_data_sewa.text.toString().trim())
            val intent = Intent(this, Invoice::class.java)
            intent.putExtras(bundel)
            //pindah aktivity
            startActivity(intent)
//            nama_data_sewa.setText("");
//            nohp_data_sewa.setText("");
//            jenis_mobil_data_sewa.setText("");
//            jenis_sewa_data_sewa.setText("");
//            hari_data_sewa.setText("");
        }
    }
}