package com.tugas.tugas

import Adapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_tambah_edit.*


class TambahEdit : AppCompatActivity() {
    private var list : MutableList<DataMobil> = ArrayList()
    private lateinit var rvData: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_edit)
        btn_tambah.setOnClickListener(){
            tambahData()
        }
        rvData = findViewById(R.id.rv_mobile)
        rvData.setHasFixedSize(true)
        getList()
    }

    private fun getList() {
        val listadapter = Adapter(this,list)
        rvData.adapter = listadapter
        rvData.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //deklarasi untuk Database
        var myRef = FirebaseDatabase.getInstance().getReference("DataMobile")
        //isi data di RV
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children) {
                    val x = snap.getValue(DataMobil::class.java)
//                    Log.e("TEST", Gson().toJson(x))
                    list.add(x!!)
                    listadapter.notifyDataSetChanged()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@TambahEdit, "Terjai kesalahan ketika inisialisasi database", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun tambahData() {
        if (nama_mobil.text.toString().trim().isEmpty()){
            nama_mobil.error = "masukkan UserName"
            nama_mobil.requestFocus()
            return
        }
        if (harga_mobile.text.toString().isEmpty()){
            harga_mobile.error ="masukkan Paswd yang benar"
            harga_mobile.requestFocus()
            return
        }
        val database = FirebaseDatabase.getInstance()
        val id_mobile =database.getReference("DataMobile").push().getKey();
        val data = DataMobil(id_mobile.toString(),nama_mobil.text.toString(), harga_mobile.text.toString())

        val myRef = database.getReference("DataMobile")

//        myRef.child(id_mobile.toString()).setValue(data)
        myRef.child(id_mobile.toString()).setValue(data).addOnCompleteListener {
            Toast.makeText(this, "Sukses Tambah data", Toast.LENGTH_LONG).show()
            nama_mobil.setText("");
            harga_mobile.setText("");
        }
    }
}