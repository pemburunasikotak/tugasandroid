
import andrePramudjianto.belajarbahasainggris.materividio.ModelMobil
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.tugas.tugas.*
import com.tugas.tugas.R
import kotlinx.android.synthetic.main.activity_data_sewa_cust.*

class Adapter (private val context: Context, private val list:List<DataMobil>)
    : RecyclerView.Adapter<Adapter.HomePemesanHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.HomePemesanHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.list_mobil, parent, false
        )
        return HomePemesanHolder(view)
    }

    override fun onBindViewHolder(holder: Adapter.HomePemesanHolder, position: Int) {
        val list = list[position]
        holder.tv_name.text = list.nama_mobil
        holder.tv_harga.text = list.harga_mobile
        holder.id.text= list.id_mobile
        holder.btn_hapus.setOnClickListener(){
            val ref = FirebaseDatabase.getInstance().getReference("DataMobile").child(list.id_mobile)
            val Query: Query = ref
            //Query database
            Query.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (appleSnapshot in dataSnapshot.children) {
                        appleSnapshot.ref.removeValue()
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
                }
            })
            val intent = Intent(context, TambahEdit::class.java)
            context.startActivity(intent)
        }
        holder.btn_edit.setOnClickListener(){
            val bundel = Bundle()
            bundel.putString("id", list.id_mobile)
            bundel.putString("nama", list.nama_mobil)
            bundel.putString("harga",list.harga_mobile )

            val intent = Intent(context, EditMobil::class.java)
            intent.putExtras(bundel)
            //pindah aktivity
            context.startActivity(intent);
        }
    }

    override fun getItemCount(): Int = list.size

    inner class HomePemesanHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name: TextView = view.findViewById(R.id.tv_nama)
        val tv_harga: TextView = view.findViewById(R.id.tv_harga)
        val btn_hapus: Button = view.findViewById(R.id.btn_hapus)
        val btn_edit: Button = view.findViewById(R.id.btn_edit)
        val id : TextView= view.findViewById(R.id.tv_id)


    }
}